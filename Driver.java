//TODO: 
/**
 * Project: THPT 3 - Date
 * Class: ITCS 1213-091
 * Lab: Section 07
 * Author: Sean Flanagan
 * Date: 3/18/2015
 * 
 * This is the Driver() class which will handle holding the predefined dates and passing them through the Date class to run calculations and output information
 * 
 * @author Sean Flanagan
 * @version 3/18/2015
 */

import java.util.Scanner;//Imports Scanner package from the utility package to take user input. 
import java.util.StringTokenizer;//Imports the StringTokenizer class from the utility package to take user input.
import java.util.Random;//Imports the Random class from the utility package to take user input.

public class Driver
{
    static String[] dt = {"3/18/2015", "4/25/1996", "1/11/1996", "7/4/1776", "9/16/2014"};//This creates the input String array
    public static void main(String [ ] args)
    {
        Random rand = new Random();//Creates new instance of the Random class.
        StringTokenizer stoke;//Creates memory reference for the StringTokenizer class.
        Scanner input = new Scanner(System.in);//Creates a new instance of the Scanner class on the keyboard.
        Date d;//Assigns memory reference for the Date class in 'd'.
        int month, day, year, i, r, jDay;//Creates all global Integer fields.
        i = 0;//Assigns field i to zero.
        char choice;//Creates all global character fields.
        boolean leap;//Creates all global boolean fields.
        
        //Prints a disclaimer to the screen regarding the Random data determination.
        System.out.println("****************PLEASE NOTE****************");
        System.out.println("The number of days to be added and subtracted are determined at random");
        System.out.println("The dates which are compared to the input dates at the end of each section are picked at random from the date string");
        System.out.println();
        
        //Asks how the user would like the output.
        System.out.println("How would you like to output the dates?");
        System.out.println("'L' - 'March 25, 2015' or 'S' - '3/25/2015': ");
        choice = input.next().charAt(0);//Takes keyboard input from Scanner to choice field.
        System.out.println();//Creates space.
        
        while(i < 5)//While loop cycles through all of the input dates
        {
            d = new Date(dt[i]);//Creates a new instance of the Date class with the String array at the current index 
            leap = d.getLeap();//Utilizes the getLeap() method to determine whether the date is a leap year or not...
            r = rand.nextInt(364)+1;//Sets variable r to a random number between 1-356.
            d.setChoice(choice);//sets the Date's choice variable.
            
            
            System.out.println("The input date is: " + d.toString());//Prints the output date
            jDay = d.getJulian();//returns the input date's Julian day
            System.out.println("It's Julian day is " + jDay);//Prints the Julian day for the input date
            if(d.getLeap() == true) {//Determines if it is a leap year for console output
                System.out.println("It is a leap year!");
            } else {
                System.out.println("It is not a leap year...");
            }
            System.out.println();//Creates space for asthetics
            
            Date dAdd = d.add(r);//Creates a new Date instance as the result of adding random number r to Date d.
            dAdd.setChoice(choice);//sets the addDate's choice variable.
            d = new Date(dt[i]);//Resets the date d with the original input.
            d.setChoice(choice);//sets the Date's choice variable.
            
            System.out.println("The date after adding " + r + " days is " + dAdd.toString());//Prints new date after adding a random number of days
            jDay = dAdd.getJulian();//Returns the Add Date's Julian day
            System.out.println("It's Julian day is " + jDay);//Prints Julian day for the add Date
            if(dAdd.getLeap() == true) {//Determines if it is a leap year for console output
                System.out.println("It is a leap year!");
            } else {
                System.out.println("It is not a leap year...");
            }
            System.out.println();//Creates space for asthetics
            
            r = rand.nextInt(364)+1;//Establishes random variable r to a random number between 1-365.
            Date dSub = d.subtract(r);
            dSub.setChoice(choice);//Sets the subDate's choice to the global choice variable.
            d = new Date(dt[i]);//Resets the date d with the original input.
            d.setChoice(choice);//sets the Date's choice variable.
            
            System.out.println("The date after subtracting " + r + " days is " + dSub.toString());//Prints the value of the subtracting value.
            jDay = dSub.getJulian();//Returns the Add Date's Julian day
            System.out.println("It's Julian day is " + jDay);//Prints Julian day for the add Date
            if(dSub.getLeap() == true) {//Determines if it is a leap year for console output
                System.out.println("It is a leap year!");
            } else {
                System.out.println("It is not a leap year...");
            }
            System.out.println();//Creates space for asthetics
            
            //Creates a new instance of date using the current index for comparison
            Date t = new Date(dt[i]);
            t.setChoice(choice);
            
            
            //The following section handles picking a date from the string at random and comparing it with the dt[i] in question
            r = rand.nextInt(5);//Generates a random Integer between 0-4.
            
            Date n = new Date(dt[r]);//Creates a new instance of the Date class with the random string.
            n.setChoice(choice);//Sets the newDate's choice variable to the globally defined choice Variable.
            
            System.out.println("There are " + t.daysBetween(n) + " days between " + t.toString() + " and " + n.toString());//Outputs the days between the two dates.
            System.out.println(t.compareTo(n));//Prints the comparison string.
            System.out.println(t.equals(n));//Prints the comparison string.
            
            System.out.println("___________________________________________________________");//Creates a line on the output screen for asthetic purposes.
            
            i++;//Increments the counter.
            
        }//end of while loop
        System.out.println("Goodbye!");//Closing outputs statement
        System.out.println();
        //Outputs Student information
        System.out.println("Project: THPT 3");
        System.out.println("Class: ITCS 1213-091");
        System.out.println("Lab: Section 07");
        System.out.println("Author: Sean Flanagan");
        System.out.println("Date: 3/18/2015");
    }//end of main()
}//end of class definition