//TODO: 
/**
 * File Name: Date.java
 * Project: THPT 3 - Date
 * Class: ITCS 1213-091
 * Professor: Lorrie Lehmann
 * Lab: Section 07
 * TA: Ed
 * 
 * Serves as a way to take input integers to create a new Date and perform numerical operations on these dates such as add, subtract and calculating the days between the 
 * dates
 * 
 * @author Sean Flanagan
 * @version 3/18/2015
 */

import java.util.StringTokenizer;//Imports the StringTokenizer class from the Java APIs Utility Package.
public class Date
{
    StringTokenizer stoke;//Creates a memory reference for the StringTokenizer class in 'stoke'.
    private int month, day, year, jDay, tmp, tmpDay2, tmpMon;//Creates all private global Integer fields.
    private String monStr, tmpStr, dStr;//Creates all private global String fields.
    private boolean leap, choi;//Creates all private boolean fields.
    private char choice;//Creates all private character fields.
    
    /**
     * Unloaded constructor which nulls all the global variables
     */
    public Date()
    {
        month = day = year = jDay = tmp = tmpDay2 = tmpMon = 0;//Sets all global Integer variables to 0.
        monStr = tmpStr = dStr = "";//Sets the String variable to a blank value.
        leap = choi = false;//Defaults the boolean value to false.
        choice = 's';//Defaults the choice character to s for short output.
    }
    
    /**
     * Loaded constructor which creates a new Date instance, with three input date variables: inMonth, inDay, and inYear and calculates whether it is a leap year and its 
     * Julian day
     * 
     * @param inMonth Integer value holding the month value
     * @param inDay Integer value holding the day value
     * @param inYear Integer value holding the year value
     */
    public Date(String inDate)
    {
        stoke = new StringTokenizer(inDate, "/");//Creates a StringTokenizer with the input date having the '/' as the delimiter.
        month = Integer.parseInt(stoke.nextToken());//Sets global month variable to tokenized input month
        day = Integer.parseInt(stoke.nextToken());//Sets global day variable to tokenized input day.
        year = Integer.parseInt(stoke.nextToken());//Sets global year variable to tokenized input year
        jDay = yDay(month, day);//Calculates Julian day passing the month and day values to the yDay() method.
        if((year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0))//If statement takes inYear to determine if it is a leap year.
        {
            leap = true;//Sets leap year to true
        } else {
            leap = false;//Sets leap year to false
        }
        if(leap == true && jDay > 59)//If statement adds one day if it is a leap year and date is greater than Feb 28.
        {
            jDay = jDay + 1;//Adds one to the Julian day variable for leap year.
        }
        validate(jDay);//runs validate() method to correct the field values.
        monStr = tmpStr;//Month String equals the temporary string from the validate() method.
        dStr = tmpStr;//This does the same as the above line. however this somehow corrects the issue of displaying incorrect month strings
        
    }
    
    /**
     * This is the add() method which takes the predefined date variables: month, day, and year and adds the input number 'inDays' and ouputs a new Date instance
     * 
     * @param inDays Integer value which accounts for the amount of days to be added to the date
     * @return New Date instance with the newly calculated date information: day1, mon1, y1.
     */
    public Date add(int inDays)
    {
        //Establishes 4 temporary variables to hold the Date's month, day, Julian day and year
        int day1 = this.day;
        int mon1 = this.month;
        int y1 = this.year;
        int jDay1 = this.jDay;
        String output = "";//Creates a temporary String field to hold an output string for creating a new Date instance.
        
        tmp = jDay + inDays;//Adds the input days to the julian day variable
        if(tmp > 365)//If statement determines if the day crosses over the New Year and adjusts the vars accordingly
        {
            tmp = tmp - 365;//Takes 365 days from the Julian day
            y1 = y1 + 1;//Adds one year to the year count
        }
        day1 = validate(tmp);//Converts the Julian day into a tangible date.
        mon1 = tmpMon;//sets the temp month variable 'mon1' to 'tmpMon' variable from the validate method.
        output = mon1 + "/" + day1 + "/" + y1;//Creates a new String with the above temporary variables and assigns it to temp String 'output'
        
        return new Date(output);//Returns new date instance with the above variables.
    }
    
    /**
     * This is the subtract() method which takes the predefined date variables: month, day, and year and subtracts the input number 'inDays' and outputs a new Date instance with the new value.
     * 
     * @param inDays Integer value which accounts for the amount of days to be subtracted from the date.
     * @return New instance of the Date program which has the calculated new date information: tmpMon, tmp, y1.
     */
    public Date subtract(int inDays)
    {
        //Establishes 4 temporary variables to hold the Date's month, day, Julian day and year
        int day1 = this.day;
        int mon1 = this.month;
        int y1 = this.year;
        int jDay1 = this.jDay;
        String output = "";//Creates a temporary String field to hold an output string for creating a new Date instance.
        
        tmp = jDay1 - inDays;// Subtracts the number of input days (inDays) with the date's yDay (tmpDay2).
        if(tmp < 0)//If statement which accounts for crossing into the previous year.
        {
            tmp = tmp * -1;//multiplies the day number by negative one to yield a positive number.
            y1 = y1 - 1;//Subtracts one from the year count to account for going back a year.
            if(tmp > 365)//Nested If statement to account for crossing more than one year backwards.
            {
                y1 = y1 - 1;//Subtracts one from the year count to account for going back a year.
            }
            tmp = 365 - tmp;//Subtracts 365 days from the day variable to determine which day of the year it is.
        }
        
        day1 = validate(tmp);//passes the calculated yDay through the validate() method to generate a tangible date and the new month value.
        mon1 = tmpMon;//sets the temp month variable 'mon1' to 'tmpMon' variable from the validate method.
        output = mon1 + "/" + day1 + "/" + y1;//Creates a new String with the above temporary variables and assigns it to temp String 'output'
        
        return new Date(output); //returns the newly generated date.
    }
    
    /**
     * This is the daysBetween() method which takes the predefined date variables: month, day and year and a new input year 'inDate' to determine how many days are between the two Dates.
     * 
     * @param inDate Date value which holds the memory location of a new Date instance.
     * @return finDay Integer value which holds the number of Days between the predefined date and the input date.
     */
    public int daysBetween(Date inDate)
    {
        //Declares the temporary integer values
        int day1, day2, mon1, mon2, y1, y2, tmpY, tmpDay1, tmpDay2, finDay;
        //The next 6 lines allocate the above temporary integers with their corresponding numbers.
        day1 = this.day;
        day2 = inDate.day;
        mon1 = this.month;
        mon2 = inDate.month;
        y1 = this.year;
        y2 = inDate.year;
        tmpY = 0;//This sets the temporary year value to 0
        
        //The following two lines calculate the yDays of the two dates.
        tmpDay1 = yDay(mon1, day1);
        tmpDay2 = yDay(mon2, day2);
        
        //This nested If statement calculates if the years are different between the two dates and yields a number of days to account for the difference.
        if(y1 > y2)
        {
            tmpY = y1 - y2;//Sets the temporary year variable to the number of years difference between the two dates.
            tmpY = tmpY * 365;//Multiplies the difference in years by 365 to determine how many days are accounted for.
        } else if(y1 == y2)
        {
            tmpY = 0;//The year values are the same therefore the tmpYear stays the same.
        } else if (y1 < y2)
        {
            tmpY = y2 - y1;//Sets the temporary year variable to the number of years difference between the two dates.
            tmpY = tmpY * 365;//Multiplies the difference in years by 365 to determine how many days are accounted for.
        }
        
        finDay = tmpDay1 - tmpDay2;//Determines the difference in days between the two dates.
        if(finDay < 0)//If statement determines if the calculated day variable is a negative and corrects for the difference.
        {
            finDay = finDay * -1;//corrects the day if it is negative.
        }
        finDay = finDay + tmpY;//Adds the tmpY value to finDay to account for the difference in years (if any).
        return finDay;//returns the above calculated variable.
    }
    
    /**
     * This method takes the contents of this Date instance and returns it in a String format for use in console output in external classes
     * 
     * @return String value holding a nicely structured date.
     */
    public String toString()
    {
         String output = getDate(choice) + day;//Constructs an output string.
         if(choi == true){//Establishes output date if it uses the "Month day, year" output.
             output = output + ", " + year;//"Month day, year" output.
         } else if(choi == false){//Establishes the output date if it uses the "month/day/year" output.
             output = output + "/" + year;//"month/day/year" output
         }
         return output;//returns the above constructed String.
    }
    
    /**
     * This method determines which date is greater.
     * 
     * @param Date instance to compare to the instance which is running this method
     * @return String value holding which date is greater than the other
     */
    public String compareTo(Date in2)
    {
        String output = "";//Creates the output String field 'output' and defaults it to a blank value.
        Integer tmp1, tmp2;//Creates 2 temporary integer values to determine the difference between the 2 dates if there is a difference of years.
        if(this.year != in2.year){//If year values are different determines and compares Julian days for the years.
            tmp1 = this.jDay + (365 * this.year);//gets Julian day with year accounted for on this date.
            tmp2 = in2.jDay + (365 * in2.year);//gets Julian day with year accounted for on input date 2.
            if(tmp1 > tmp2){//if Year Julian1 is greater than Year Julian2 set output to be greater.
                output = this.toString() + " is greater than " + in2.toString();//sets the 'output' field to say it is greater than second input.
            } else if(tmp1 < tmp2){//if Year Julian1 is less than Year Julian2 set the output to be less.
                output = this.toString() + " is less than " + in2.toString();//sets the 'output' field to say it is less than second input.
            } else {
                output = "The dates are the same";//sets the 'output' field to 'the dates are the same' String
            }
        } else if(this.year == in2.year){//If year values are the same this compares the Julian days already determined.
            if(this.jDay > in2.jDay){//If this day is greater.
                output = this.toString() + " is greater than " + in2.toString();//sets the 'output' field to say it is greater than the second input
            } else if(this.jDay < in2.jDay){//If this day is less than.
                output = this.toString() + " is less than " + in2.toString();//sets the 'output' field to say it is less than the second input.
            } else {
                output = "The dates are the same.";//sets the 'output' field to 'the dates are the same' String.
            }
        }
        return output;//returns the above constructed String.
    }
    
    /**
     * This method determines if the two dates are equal to one another.
     * 
     * @param Date instance to compare to the instance which is running this method.
     * @return String value holding whether the two dates are equal or not.
     */
    public String equals(Date in2)
    {
        String output = "";//Creates the output String field 'output' and defaults it to a blank value.
        if(this.jDay == in2.jDay){//If this day is equal to input day
            output = this.toString() + " is equal to " + in2.toString();//sets the 'output' field to say it is equal to the second input.
        } else {//If this day is not equal to input day.
            output = this.toString() + " is not equal to " + in2.toString();//sets the 'output' field to say it is not equal to the second input.
        }
        return output;//returns the above constructed String 'output'
    }
    
    /**
     * This get() method takes the input choice character and determines what the date structure should be
     * 
     * @param choice Character input which determines the output of the date
     */
    public String getDate(char choice)
    {
        String output = "";//Creates the output String field 'output' and defaults it to a blank value.
        if(choice == 'S' || choice == 's')//If input choice is for Short output
        {
            output = month + "/";//Constructs the output String to be "month/day"
            choi = false;//Sets global boolean 'choi' to false
        } else if(choice == 'L' || choice == 'l')//If input choice is for Long output.
        {
            output = dStr + " ";//Constructs the output String to be "Month Day"
            choi = true;//Sets global boolean 'choi' to true
        }
        return output;//Returns the above constructed String 'output'
    }
    
    /**
     * This get() method simply returns the boolean leap field to an external class
     * 
     * @return Boolean value holding whether the date is a leap year or not.
     */
    public boolean getLeap()
    {
        return leap;
    }
    
    /**
     * This get() method simply returns the Integer value for Julian Days to an external class
     * 
     * @return Integer value holding the Julian day for the date.
     */
    public int getJulian()
    {
        return jDay;
    }
    
    /**
     * This set() method simply sets the global choice variable with an external input
     * 
     * @param choice Character input which determines the output of the toString()
     */
    public void setChoice(char inChoice)
    {
        choice = inChoice;//sets the choice variable to the input choice varaible.
    }
    
     /**
     * This method determines the yDay, or day of the year, for the date based on the input month and day variables accounting for how many days have passed for each month leading up to the date. This yDay value makes future calculation easier.
     * 
     * @param month Integer value holding the month value for the input date.
     * @param day Integer value holding the month value for the input date.
     * @return nDay Integer value holding the new day after the number of days accounting for passed months.
     */
    public int yDay(int month, int day)
    {
        int nDay = 0;//Sets the temporary nDay variable to 0.
        //Switch statement takes month value as input to determine which month the date falls on, adding the number of passed days leading to that month to the temporary day value.
        switch (month) {
            case 1:  monStr = "January";
                       nDay = day;
                       break;
            case 2:  monStr = "February";
                       nDay = day + 31;
                       break;
            case 3:  monStr = "March";
                       nDay = day + 59;
                       break;
            case 4:  monStr = "April";
                       nDay = day + 90;
                       break;
            case 5:  monStr = "May";
                       nDay = day + 120;
                       break;
            case 6:  monStr = "June";
                       nDay = day + 151;
                       break;
            case 7:  monStr = "July";
                       nDay = day + 181;
                       break;
            case 8:  monStr = "August";
                       nDay = day + 212;
                       break;
            case 9:  monStr = "September";
                       nDay = day + 243;
                       break;
            case 10: monStr = "October";
                       nDay = day + 273;
                       break;
            case 11: monStr = "November";
                       nDay = day + 304;
                       break;
            case 12: monStr = "December";
                       nDay = day + 334;
                       break;
            default: monStr = "not working";
                     break;
        }
        return nDay;//returns the yDay value.
    }
    
    /**
     * This method takes the integer input of tmpDay with the intent of converting a yDay into a date day value. Additionally this method accounts for the change in months and potentially years.
     * 
     * @param tmpDay Integer value accounting for the day of the year variable.
     * @return tmpDay Integer value accounting for the newly calculated day.
     */
    public int validate(int tmpDay)
     {
         tmpStr = "";
         //If statement calculates if the day is less than 31 in which case the month is January.
         if(tmpDay < 31){
            tmpStr = "January";
            tmpMon = 1;
         }
         //Sequentially nested if statements to account for what the date is.
          if(tmpDay > 365){
             //If tmpDay is greater than 365 a year has passed and is accounted for.
             year = year + 1;
             tmpDay = tmpDay - 365;
             tmpStr = "January";
             tmpMon = 1;
          } else if (tmpDay > 334){
             tmpDay = tmpDay - 334;
             tmpStr = "December";
             tmpMon = 12;
          } else if (tmpDay > 304){
             tmpDay = tmpDay - 304;
             tmpStr = "November";
             tmpMon = 11;
          } else if (tmpDay > 273){
             tmpDay = tmpDay - 273;
             tmpStr = "October";
             tmpMon = 10;
          } else if (tmpDay > 243){
             tmpDay = tmpDay - 243;
             tmpStr = "September";
             tmpMon = 9;
          } else if (tmpDay > 212){
             tmpDay = tmpDay - 212;
             tmpStr = "August";
             tmpMon = 8;
          } else if (tmpDay > 181){
             tmpDay = tmpDay - 181;
             tmpStr = "July";
             tmpMon = 7;
          } else if (tmpDay > 151){
             tmpDay = tmpDay - 151;
             tmpStr = "June";
             tmpMon = 6;
          } else if (tmpDay > 120){
             tmpDay = tmpDay - 120;
             tmpStr = "May";
             tmpMon = 5;
          } else if (tmpDay > 90){
             tmpDay = tmpDay - 90;
             tmpStr = "April";
             tmpMon = 4;
          } else if (tmpDay > 59){
             tmpDay = tmpDay - 59;
             tmpStr = "March";
             tmpMon = 3;
          } else if (tmpDay > 31){
             tmpDay = tmpDay - 31;
             tmpStr = "February";
             tmpMon = 2;
         } 
         return tmpDay;//returns the calculated date.
    }
}