# README #

* AUTHORS: Sean Flanagan
* SCHOOL: University of North Carolina at Charlotte
* CLASS: ITCS 1213-091
* LAB: Section 07
* PROFESSOR: Lorrie Lehmann
* PROJECT TITLE: Take Home Programming Test 3 - Date.java
* PURPOSE OF PROJECT: To take a String input of a date in "3/18/2015" format and create a date to perform add, subtract and comparison operations on it
* VERSION: 1.0
* DATE: 3/18/2015
* HOW TO START THIS PROJECT: Run through Java Compiler and create instance of the main() method in Driver.class

### How do I get set up? ###

* Download git
* Compile the .java files with javac compiler
* Java 7 JRE & JDK
* Run the driver.java to test the program

### Contribution guidelines ###

* Under no circumstances is this code to be used without proper citation.
* You may only use this un-cited if you simply use it for logical understanding

### Who do I talk to? ###

* @sflanag5

### Download Here ###

| [Download](https://bitbucket.org/sflanag5/thpt3-flanagan/downloads/THPT3.jar) |
________________________________________________________________________________