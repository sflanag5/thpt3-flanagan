AUTHORS: Sean Flanagan
SCHOOL: University of North Carolina at Charlotte
CLASS: ITCS 1213-091
LAB: Section 07
PROFESSOR: Lorrie Lehmann
PROJECT TITLE: Take Home Programming Test 3 - Date.java
PURPOSE OF PROJECT: To take a String input of a date in "3/18/2015" format and create a date to perform add, subtract and comparison operations on it
VERSION: 1.0
DATE: 3/18/2015
HOW TO START THIS PROJECT: Run through Java Compiler and create instance of the main() method in Driver.class
